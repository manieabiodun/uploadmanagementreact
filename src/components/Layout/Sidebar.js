import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Sidebar extends Component {
	render() {
		return (
			<div
				className="sidebar"
				data-color="purple"
				data-background-color="white"
				data-image="../assets/img/sidebar-1.jpg"
			>
				<div className="logo">
					<a href="http://www.creative-tim.com" className="simple-text logo-normal">
						Creative Tim
					</a>
				</div>
				<div className="sidebar-wrapper">
					<ul className="nav">
						<li>
							<Link className="nav-link" to={`/dashboard`}>
								<i className="material-icons">dashboard</i>
								<p>Dashboard</p>
							</Link>
						</li>
						<li>
							<Link className="nav-link" to={`/addDirectory`}>
								<i className="material-icons">create_directory</i>
								<p>Create Directory</p>
							</Link>
						</li>

						<li className="nav-item ">
							<Link className="nav-link" to={`/addFile`}>
								<i className="material-icons">add_file</i>
								<p>Add File</p>
							</Link>
						</li>
						<li className="nav-item ">
							<Link className="nav-link" to={`/viewFile`}>
								<i className="material-icons">bubble_chart</i>
								<p>View Files</p>
							</Link>
						</li>
					</ul>
				</div>
			</div>
		);
	}
}

export default Sidebar;
