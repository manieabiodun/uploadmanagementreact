import React, { Component } from 'react';
// import CreateTaskButton from './Task/CreateTaskButton';
import { getFiles } from '../../actions/fileActions';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class ViewFile extends Component {
	componentDidMount() {
		this.props.getFiles();
	}
	render() {
		return (
			<div className="content">
				<div className="container-fluid">
					<div className="row">
						<div className="col-md-12">
							<div className="card card-plain">
								<div className="card-header card-header-primary">
									<h4 className="card-title mt-0"> All Files</h4>
									<p className="card-category"> A list of all available file</p>
								</div>
								<div className="card-body">
									<div className="table-responsive">
										<table className="table table-hover">
											<thead className="">
												<tr>
													<th>ID</th>
													<th>File Name</th>

													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>hey</td>
													<td>
														<Link to={`/updateFile/${1}`}>
															<button className="btn btn-primary btn-xs">Update</button>
														</Link>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
ViewFile.propTypes = {
	file: PropTypes.object.isRequired,
	getFiles: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	file: state.file
});

export default ViewFile;
