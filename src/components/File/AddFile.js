import React, { Component } from 'react';
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';

class AddFile extends Component {
	constructor() {
		super();
	}

	render() {
		return (
			<div className="content">
				<div className="container-fluid">
					<div className="row">
						<div className="col-md-8">
							<div className="card">
								<div className="card-header card-header-primary">
									<h4 className="card-title">Add File</h4>
									<p className="card-category">Fill the form below</p>
								</div>
								<div className="card-body">
									<form onSubmit={this.onSubmit}>
										<div className="col-md-12">
											<FilePond server="http://127.0.0.1:8000/api/file" />
										</div>

										<div className="clearfix" />
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default AddFile;
