import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createDirectory } from '../../actions/directoryActions';

class AddDirectory extends Component {
	constructor() {
		super();
		this.state = {
			name: ''
		};
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.errors) {
			this.setState({ errors: nextProps.errors });
		}
	}

	onChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	onSubmit(e) {
		e.preventDefault();
		const newDirectory = {
			name: this.state.name
		};

		this.props.createDirectory(newDirectory, this.props.history);
	}

	render() {
		const { errors } = this.state;
		return (
			<div className="content">
				<div className="container-fluid">
					<div className="row">
						<div className="col-md-8">
							<div className="card">
								<div className="card-header card-header-primary">
									<h4 className="card-title">Add AddDirectory</h4>
									<p className="card-category">Fill the form below</p>
								</div>
								<div className="card-body">
									<form onSubmit={this.onSubmit}>
										<div className="col-md-12">
											<div className="form-group">
												<label className="bmd-label-floating">Folder Name</label>
												<input
													type="text"
													className="form-control"
													name="name"
													value={this.state.name}
													onChange={this.onChange}
												/>
											</div>
										</div>

										<button type="submit" className="btn btn-primary pull-right">
											Save
										</button>
										<div className="clearfix" />
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

AddDirectory.propTypes = {
	createDirectory: PropTypes.func.isRequired,
	errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	errors: state.errors
});

export default connect(mapStateToProps, { createDirectory })(AddDirectory);
