import { combineReducers } from 'redux';
import directoryReducer from './directoryReducer';
import errorReducer from './errorReducer';

export default combineReducers({
	errors: errorReducer,
	directory: directoryReducer
});
