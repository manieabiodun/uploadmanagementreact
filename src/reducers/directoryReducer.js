import { GET_DIRECTORIES, GET_DIRECTORY } from '../actions/types';

const initialState = {
	directories: [],
	directory: {}
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_DIRECTORIES:
			return {
				...state,
				tasks: action.payload
			};
		default:
			return state;
	}
}
