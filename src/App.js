import React, { Component } from 'react';
import './App.css';
import './assets/css/material-dashboard.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Sidebar from './components/Layout/Sidebar';
import Header from './components/Layout/Header';
import Footer from './components/Layout/Footer';
import Dashboard from './components/Dashboard';
import ViewFile from './components/File/ViewFile';
import AddDirectory from './components/Directory/AddDirectory';
import AddFile from './components/File/AddFile';
import store from './store';
import { Provider } from 'react-redux';

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Router>
					<div>
						<Sidebar />
						<div class="main-panel">
							<Header />
							<Switch>
								<Route exact path="/" component={Dashboard} />
								<Route exact path="/dashboard" component={Dashboard} />
								<Route exact path="/addDirectory" component={AddDirectory} />
								<Route exact path="/addFile" component={AddFile} />
								<Route exact path="/viewFile" component={ViewFile} />
							</Switch>
							<Footer />
						</div>
					</div>
				</Router>
			</Provider>
		);
	}
}

export default App;
