import axios from 'axios';
import { GET_ERRORS, GET_FILES } from './types';

export const getFiles = () => async (dispatch) => {
	const res = await axios.get('http://127.0.0.1:8000/api/files');

	dispatch({
		type: GET_FILES,
		payload: res.data
	});
};
